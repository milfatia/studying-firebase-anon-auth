import * as fcc from "./firebase_client_credential";

export const environment = {
  production: true,

  firebaseClientCredential: fcc.credential
};
