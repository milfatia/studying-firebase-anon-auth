import { FormControl, Validators } from "@angular/forms";
import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { IUXContent } from "./interfaces";


@Component({
  selector: "goship-editor-dialog",
  template: `
  <h3>え、何？ この黒歴史消したいの？ (´_ゝ｀)</h3>
  <hr />
  <div class="">
    <div>
      <li class="stackable"><ul *ngFor="let x of order.children">{{x.data}}</ul></li>
    </div>
  </div>
  <br />
  <br />


  <mat-form-field class="wide-form">
    <input matInput placeholder="(血の盟約に従い、我、汝を召喚する………)" [formControl]="fc.chatbox" required> 
    <mat-error *ngIf="fc.chatbox.invalid">{{getErrorMessage()}}</mat-error>
  </mat-form-field>
  <button mat-raised-button
    color="primary"
    (click)="modifyNow()"
    matTooltip="過去を変えてしまいますが、よろしいですか？"
    [disabled]="!fc.chatbox.valid">間違っていたのは俺じゃない、世界の方だ!</button>
  <br>
  <br>
  <button mat-raised-button
    color="accent"
    (click)="tryVanish()"
    matTooltip="削除してしまいます"
    [disabled]="order.owner.isAnonymous">闇の炎に抱かれて消えろ！！！</button>
    <div class="reason" *ngIf="reason">{{reason}}</div>
  <br>
  <br>
  <button mat-raised-button
    color="warn"
    matTooltip="あきらめましょう..."
    (click)="retireNow()">認めたくないものだな、若さゆえの過ちとは...</button>
  `,
  styles: [`
    .wide-form {
      min-width: 420px;
    }

    .reason {
      color: red;
      font-weight: bold;
    }
  `]
})
export class GoshipEditorDialog implements OnInit {
  /* internal */
  protected readonly targetIndex = 2;

  /* ui bounds */
  fc = {
    chatbox: new FormControl('', [Validators.required, Validators.maxLength(4096)]),
  };
  reason: string;

  /* */
  constructor(
    protected dialog: MatDialogRef<GoshipEditorDialog>,
    @Inject(MAT_DIALOG_DATA) public order: IUXContent
  ) { }


  /** */
  ngOnInit() {
    this.fc.chatbox.setValue(this.order.children[this.targetIndex].data);
    if (this.order.owner.isAnonymous) {
      this.reason = "御主人、それには「真名」が必要でございまして...";
    }
  }


  /**
   * ================================================================================================================
   * 何もせずクローズ
   * 
   * 
   * ================================================================================================================
   */
  retireNow() {
    this.dialog.close();
  }


  /**
   * ================================================================================================================
   * 改変内容を返却してクローズ
   * 
   * 
   * ================================================================================================================
   */
  modifyNow() {
    this.order.children[this.targetIndex].data = this.fc.chatbox.value;
    this.dialog.close(this.order);
  }



  /**
   * ================================================================================================================
   * 削除フラグを立てて内容を返却してクローズ
   * 
   * 
   * ================================================================================================================
   */  tryVanish() {
    confirm(`黒歴史ってのはそう簡単に消せないんだな(´_ゝ｀)ﾌﾌﾌ...`);
    this.order.removed = "true";
    this.dialog.close(this.order);
  }
  

  /** form validation error resolver */
  getErrorMessage() {
    return this.fc.chatbox.hasError('required') ? '「修正」の内容をご指定下さいませ' :
      this.fc.chatbox.hasError('maxlength') ? 'その修正量では世界が崩壊してしまいますゆえ...' : '';
  }

}