import { FormControl, Validators } from "@angular/forms";
import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { SupportAuthType } from "./interfaces"

/** form selectorに使う. */
interface IAuthProvider {
  name: string;
  value: string;
  disabled: boolean;
}

// ==================================================================================================================
// 登録とログインがほぼ似ているので共通化。名前はRegisterationDialogのままなので注意。
// 
// ==================================================================================================================
@Component({
  selector: "registeration-dialog",
  template: `
  <p *ngIf="param === 'login'">ログインには以下の情報をくだされ...</p>
  <p *ngIf="param !== 'login'">本番登録には以下の情報をくだされ...</p>
  <hr />
  <mat-form-field class="wide-form" *ngIf="param !== 'login'">
    <input matInput
      placeholder="ニックネーム(省略すると匿名さんになります)"
      [formControl]="fc.name">
    <mat-error *ngIf="fc.name.invalid">{{getNameErrorMessage()}}</mat-error>
  </mat-form-field>
  <br *ngIf="param !== 'login'"/>

  <mat-form-field class="wide-form">
    <input matInput
      placeholder="メールアドレス"
      [formControl]="fc.emailpass.email"
      required> 
    <mat-error *ngIf="fc.emailpass.email.invalid">{{getEmailpassEmailErrorMessage()}}</mat-error>
  </mat-form-field>
  <br />

  <mat-form-field class="wide-form">
    <input matInput
      type="password"
      placeholder="パスワード(6文字以上)"
      [formControl]="fc.emailpass.password"
      required>
    <mat-error *ngIf="fc.emailpass.password.invalid">{{getEmailpassPasswordErrorMessage()}}</mat-error>
  </mat-form-field>
  <br />

  <mat-form-field class="wide-form">
    <mat-select placeholder="認証情報を選んでね" [(ngModel)]="selectedProvider" [value]="selectedProvider">
      <mat-option *ngFor="let p of providers" value="{{p.value}}" [disabled]="p.disabled">{{p.name}}</mat-option>
    </mat-select>
  </mat-form-field>
  <br />
  <br />
  <button mat-raised-button color="primary" (click)="tryRegister()"
    [disabled]="!isRegisterable()" *ngIf="param !== 'login'">登録する</button>
  <button mat-raised-button color="primary" (click)="tryRegister()"
    [disabled]="!isRegisterable()" *ngIf="param === 'login'">ログイン</button>
  <button mat-raised-button color="warn" (click)="retireNow()">やっぱやめた</button>
  `,
  styles: [`
    .wide-form {
      min-width: 420px;
    }
  `]
})
export class RegisterationDialog {
  /* ui bounds */
  readonly providers = Array<IAuthProvider>(
    { name: "email/password方式", value: SupportAuthType.EMAIL_PASSWORD, disabled: false },
    { name: "google認可で登録させる(dummy)", value: SupportAuthType.GOOGLE_OAUTH, disabled: true },
    { name: "twitter認可で登録させる(dummy)", value: SupportAuthType.TWITTER_OAUTH, disabled: true },
    { name: "電話認証で登録させる(dummy)", value: SupportAuthType.PHONE_AUTH, disabled: true }
  );
  selectedProvider = SupportAuthType.EMAIL_PASSWORD;
  fc = {
    name: new FormControl('', [Validators.maxLength(255)]),
    emailpass: {
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    }
  };

  /* */
  constructor(
    protected dialog: MatDialogRef<RegisterationDialog>,
    @Inject(MAT_DIALOG_DATA) public param: string
  ) { }



  /**
   * ================================================================================================================
   * 登録(or ログイン入力)内容を返却してクローズ
   * 
   * 
   * ================================================================================================================
   */
  tryRegister() {
    let ctx: { [k: string]: any } = {};
    ctx.name = this.fc.name.value || "";
    switch (this.selectedProvider) {
      case SupportAuthType.EMAIL_PASSWORD:
        ctx.provider = this.selectedProvider;
        ctx.email = this.fc.emailpass.email.value;
        ctx.password = this.fc.emailpass.password.value;
        break;
      case SupportAuthType.GOOGLE_OAUTH:
      case SupportAuthType.TWITTER_OAUTH:
      case SupportAuthType.PHONE_AUTH:
    }
    this.dialog.close(ctx);
  }


  /**
   * ================================================================================================================
   * 何もせずクローズ
   * 
   * 
   * ================================================================================================================
   */
  retireNow() {
    this.dialog.close();
  }


  /** form validator wrapper */
  isRegisterable(): boolean {
    switch (this.selectedProvider) {
      case SupportAuthType.EMAIL_PASSWORD:
        return this.fc.emailpass.email.valid && this.fc.emailpass.password.valid;
      case SupportAuthType.GOOGLE_OAUTH:
      case SupportAuthType.TWITTER_OAUTH:
      case SupportAuthType.PHONE_AUTH:
        break;
      default:
        console.warn(`?: ${this.selectedProvider}`);
    }
    return false;
  }


  /** form validation error resolver */
  getNameErrorMessage() {
    return this.fc.name.hasError('maxlength') ? '長すぎやねん...どこの世界の奴だよ' : '';
  }


  /** form validation error resolver */
  getEmailpassEmailErrorMessage() {
    return this.fc.emailpass.email.hasError('required') ? 'メールアドレスを書きましょう' :
      this.fc.emailpass.email.hasError('email') ? 'それメアドじゃなくね？' : '';
  }


  /** form validation error resolver */
  getEmailpassPasswordErrorMessage() {
    return this.fc.emailpass.password.hasError('required') ? 'パスワードを書きましょう' :
      this.fc.emailpass.password.hasError('minlength') ? '最低6文字必要ですー' : '';
  }

}