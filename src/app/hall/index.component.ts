import { Component, AfterViewInit, OnDestroy, OnInit } from "@angular/core";
import { FirebaseService } from "../firebase/index.service";
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import { trigger, style, state } from "@angular/animations";
import { MatDialog } from "@angular/material";
import { RegisterationDialog } from "./registeration-dialog.component";
import { SupportAuthType, IUXContent, IGoshipSchema } from "./interfaces";
import { GoshipDialog } from "./goship-dialog.component";
import { GoshipEditorDialog } from "./goship-editor-dialog.component";

/** MVVM要素である、このコンポーネントのコンテキスト達を集約した型定義 */
interface IHallContext {
  title: string;
  user?: firebase.User;
  username: string;
  userId: string;
  lcd: {
    text: string,
    state: LCDLevel
  },
  enableRegisterer: boolean;
  persistentMode: "NONE" | "SESSION" | "LOCAL";
  contentsLimits: number;
}

/** アニメーション判定用 */
enum LCDLevel {
  STANDARD = "",
  WARNING = "warn",
  ERROR = "error"
}

// ==================================================================================================================
// メインホール
// 
// ==================================================================================================================
@Component({
  selector: "hall-index",
  template: `
  <div *ngIf="!ctx.userId">
    <div>えーこちら、会員制になっております。</div>
    <button mat-stroked-button color="primary" (click)="login()">ログインする</button>
    <br />
    <br />
    <button mat-stroked-button color="accent"
      (click)="tryNow()">まあ、まて。とりあえずちょっと匿名で試したい</button>
    <br />
    <br />

    <button mat-raised-button
      (click)="changePersistentMode('NONE')"
      [disabled]="ctx.persistentMode === 'NONE'"
      matTooltipPositon="below"
      matTooltip="メモリにしか残さないのでページ遷移したらアウトな方式"
      color="primary">認証をどこにも記憶しない</button>
    <button mat-raised-button
      (click)="changePersistentMode('SESSION')"
      [disabled]="ctx.persistentMode === 'SESSION'"
      matTooltipPositon="below"
      matTooltip="ブラウザセッションがなくなったら消滅の方式(タブ複製とかNW変化とか許容)"
      color="primary">セッション切れるまで</button>
    <button mat-raised-button
      (click)="changePersistentMode('LOCAL')"
      [disabled]="ctx.persistentMode === 'LOCAL'"
      matTooltipPositon="below"
      matTooltip="ブラウザ閉じてもアプリ落としても残る方式"
      color="primary">ずっとこのマシンに残せ</button>
  </div>

  <h3>{{ctx.title}}</h3>

  <div>やあ 会員ID({{ctx.userId||'なし'}}) の {{ctx.username||'名無し'}} さん</div>
  <div class="lcd" *ngIf="ctx.lcd.text" [@lcdState]="ctx.lcd.state">{{ctx.lcd.text}}</div>
  <hr />
  <button *ngIf="ctx.userId" mat-raised-button color="warn" (click)="logoff()">すまん、入る所を間違えた...</button>
  <div *ngIf="ctx.userId">
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <button mat-raised-button color="primary" (click)="bindAnything()">うわさ話しようぜ！(Firestoreへの書込)</button>
  </div>
  <div *ngIf="ctx.enableRegisterer">
    <div>&nbsp;</div>
    <button mat-raised-button color="primary" (click)="registerNow()">今登録しちゃう？しちゃう？</button>
    <div>&nbsp;</div>
  </div>

  <br />
  <h3 style="text-align:left">内部ステータス変化的なイベントログ</h3>
  <mat-list role="list">
    <mat-list-item
      role="listitem"
      class="content"
      *ngFor="let content of contents; let idx = index"
      [@removeState]="content.removed"
      [matTooltipPosition]="content.tooltipPosition||'below'"
      [matTooltip]="content.tooltip"
      (click)="onGoshipInteracted(idx)">
      <div>
        <li class="stackable">
          <ul>{{idx+1}}</ul>
          <ul [ngClass]="content.customClass">{{content.data}}</ul>
          <ul *ngFor="let c of content.children">{{c.data}}</ul>
        </li>
      </div>
    </mat-list-item>
  </mat-list>
  `,
  styles: [`
    .lcd {
      color: white;
      text-align: center;
      background-color: rgba(0, 0, 0, 0.5);
    }

    .content {
      border: 1px solid rgba(64, 0, 0, 0.5);
      border-radius: 12px;
    }

  `],
  animations: [
    trigger("lcdState", [
      state("void", style({ "background-color": "transparent" })),
      state(LCDLevel.WARNING, style({ "background-color": "orange" })),
      state(LCDLevel.ERROR, style({ "background-color": "red" }))
    ]),
    trigger("removeState", [
      state("void", style({ "background-color": "transparent" })),
      state("true", style({ "background-color": "rgba(64,0,0,0.34)" })),
    ])
  ],
})
export class HallComponent implements AfterViewInit, OnInit, OnDestroy {
  /* internal */
  protected subscribers = Array<firebase.Unsubscribe>();
  protected user: firebase.User;

  /* ui bounds */
  ctx: IHallContext = {
    title: "メインホール",
    username: "",
    userId: "",
    lcd: {
      text: "",
      state: LCDLevel.STANDARD
    },
    enableRegisterer: false,
    persistentMode: "SESSION",
    contentsLimits: 1000
  }
  contents = Array<IUXContent>();

  /* */
  constructor(
    protected fb: FirebaseService,
    protected dialog: MatDialog
  ) {}


  /* */
  ngOnInit() {
    // --------------------------------------------------------------------------------------------------------------
    // [認証変化トリガー]
    // 基本リアクティブに行うのは、シーンを要求されない処理に限定しないとタイミング制御に苦しむので注意
    // --------------------------------------------------------------------------------------------------------------
    this.addContents({ data: "➡ Firebase認証を呼ぶと勝手に恒久化セッション見つけ出してオートログイン" });
    this.subscribers.push(this.fb.auth.onAuthStateChanged(
      user => {
        this.ctx.enableRegisterer = false;
        this.user = user;
        this.ctx.username = !user ? "名無し" : user.isAnonymous ? "(匿名)" : user.displayName;
        if (!user) {
          this.ctx.userId = "";
          this.addContents({ data: "← no auth" });
          this.writeLCD("");
          console.debug("未ログインの非認証状態のUserのようです");
          return;
        }
        this.ctx.userId = user.uid;
        this.watchCollection();

        if (user.isAnonymous) {
          this.addContents({ data: "🔥 <rx:onAuthStateChanged>", children: [ { data: "匿名ログイン状態と解決された" } ] });
          this.writeLCD("あなたは今、匿名でログインしています");
          this.ctx.enableRegisterer = true;
          console.debug(` anon-logon  > ${user.uid}`, JSON.stringify(user.metadata));
        } else {
          this.addContents({ data: "🔥 <rx:onAuthStateChanged>", children: [ { data: "認証ユーザと解決された" } ] });
          this.writeLCD("あなたは今、メンバーでログインしています");
          console.debug(` member-logon> ${user.uid}`, JSON.stringify(user.metadata));
        }
      },
      e => {
        switch (e.code) {
          case "auth/invalid-api-key":
            this.writeLCD(`(環境) firebase のapikeyとかの設定がされてないっぽいよ`, LCDLevel.ERROR);
            break;
          // case "auth/xxxxxとかでいじりたいなら":
          default:
            this.writeLCD(`何かコケたようで...: ${e.code} (${e.message})`, LCDLevel.WARNING);
        }
        this.addContents({ data: `✖ 問題あり`, children: [{data: `${e.code}`}, {data:`${e.message}`}] });
      }
    ));
  }


  /* */
  ngAfterViewInit() {
  }


  /* */
  ngOnDestroy() {
    for (const s of this.subscribers) {
      try {
        s();
      } catch (e) {
        console.warn(`?: ${e.message}`, e);
      }
    }
  }


  /** testable wrapper */
  protected now() {
    return Date.now();
  }


  /** 情報表示パネル用helper */
  protected writeLCD(message: string, level = LCDLevel.STANDARD) {
    this.ctx.lcd.text = message;
    this.ctx.lcd.state = level;
  }


  /** add and cleanup helper */
  protected addContents(c: IUXContent) {
    const s = this.contents.push(c);
     for (let i = 0; i<this.ctx.contentsLimits - s; i++) {
      this.contents.shift;
    }
  }


  /** update helper */
  protected updateContents(c: IUXContent) {
    const prev = this.contents.find(x => x.id && c.id && x.id === c.id);
    if (prev) {
      prev.children = c.children;
    }
  }


  /** remove helper */
  protected removeContents(c: IUXContent) {
    const prev = this.contents.find(x => x.id && c.id && x.id === c.id);
    if (prev) {
      prev.removed = "true";
    }
  }


  // ================================================================================================================
  // Firestoreコレクションのリアルタイムトラッキング
  // 
  // 噂のライフサイクルに合わせてリアクティブに反応するよ
  // ================================================================================================================
  protected watchCollection() {
    this.addContents({ data: "➡ Firestoreの goships コレクションをモニタリング開始" });
    const db = this.fb.firestore.collection("goships");
    db.onSnapshot(
      snapshot => {
        for (const d of snapshot.docChanges().sort((a, b) => a.doc.data(
          { serverTimestamps: "previous"})["ts"] - b.doc.data({ serverTimestamps: "previous"})["ts"])
        ) {
          // これclosureでキャプチャするのでforの外に出すとイテレーションバグを生むので注意
          const x: IGoshipSchema = d.doc.data({ serverTimestamps: "estimate" }) as any as IGoshipSchema;
          const y: IUXContent = {
            id: d.doc.id,
            data: "🔥 <goship>",
            children: [
              { data: `[${new Date(x.ts).toLocaleString()}]` },
              { data: `${x.from} ➡ ${x.to}:` },
              { data: `${x.text}` },
              { data: `<${x.sns||""}>` }
            ],
            owner: {
              isAnonymous: x.isAnonymous,
              uid: x.owner,
              username: x.from
            }
          };
          switch (d.type) {
            // ------------------------------------------------------------------------------------------------------
            // 新規追加(localオフラインキューでもよい)
            // ------------------------------------------------------------------------------------------------------
            case "added":
              // owner guard(first)
              if (y.owner && y.owner.uid === this.ctx.userId) {
                y.tooltipPosition = "below";
                y.tooltip = "これ自分の発言だ！(クリックすると...)";
                y.onClick = async () => await this.toggleGoshipEditor(y);
              } else if (y.owner && y.owner.isAnonymous) {
                y.tooltipPosition = "below";
                y.tooltip = "どっかの誰かの発言(´_ゝ｀)";
              } else {
                y.tooltipPosition = "below";
                if (y.owner.username === "匿名") {
                  y.tooltip = `これは(ID: ${y.owner.uid})さんの噂ですねぇ...でも何で匿名にしているんだろう(*''ω''*)`;
                } else {
                  y.tooltip = `これは(ID: ${y.owner.uid})さんの噂ですねぇ`;
                }
              }
              this.addContents(y);
              break;
            // ------------------------------------------------------------------------------------------------------
            // 既存編集(metadataは除き、timestampは確定したモノのみ)
            // ------------------------------------------------------------------------------------------------------
            case "modified":
              // セキュリティルールの効果測定のために匿名更新はUI上から試みれるようにしている(削除はダメ)
              // ただし、失敗するのでこのイベントは失敗したというのを判定できるようにしなければならない.
              // それを補足するのが困難なので、試みたDocのisAnonymousフィールドが用意されていれば活用できる.
              // なお、SDKは複数回試みるので、この処理も比例して呼ばれる
              if (x.isAnonymous && this.user.isAnonymous) {
                this.addContents({
                  data: "← ✖ 匿名では権限がありませぬ...",
                  children: [{data: `docId: ${y.id}` }, {data: `${x.from}(${x.owner})のエントリ`}]
                });
                return;
              }
              this.updateContents(y);
              this.addContents({
                data: "← firestoreのdocumentがupdateされたようです",
                children: [{data: `docId: ${y.id}` }, {data: `${x.from}(${x.owner})のエントリ`}]
              });
              break;
            // ------------------------------------------------------------------------------------------------------
            // 消滅(確定したモノのみ)
            // ------------------------------------------------------------------------------------------------------
            case "removed":
              this.removeContents(y);
              this.addContents({
                data: "← firestoreのdocumentが消されたようです...",
                children: [{data: `docId: ${y.id}` }, {data: `${x.from}(${x.owner})のエントリ`}]
              });
              break;
          }
        }
      },
      e => {
        if (!this.user) {
          console.debug(`非認証Userはfirestoreにアクセスできないのでinsufficient permissionsというのは正常: ${e.message}`);
          return;
        }
        this.writeLCD(`何かコケたようで...: ${e.message}`, LCDLevel.WARNING);
        this.addContents({ data: `✖ 問題あり`, children: [{data:`${e.message}`}] });
      }
    );
  }


  /**
   * ================================================================================================================
   * ログアウト処理
   * 
   * Firebaseの機能でログアウトするだけだが、匿名はセッションを失う
   * ================================================================================================================
   */
  async logoff() {
    this.addContents({ data: "➡ auth.signOut()をawait" });
    await this.fb.auth.signOut();
  }


  /**
   * ================================================================================================================
   * ログイン処理
   * 
   * Dialogを使ってログイン処理(登録用と共有)
   * Dialog側でFirebaseを実行せず、コントロールは結果を受け取ってこちら側で行う
   * ================================================================================================================
   */
  async login() {
    const reg = this.dialog.open(RegisterationDialog, {
      disableClose: true, hasBackdrop: true, minWidth: "480px", data: "login" });
    const x = reg.afterClosed().subscribe(
      async s => {
        // canceled
        if (!s || !s.provider) {
          return;
        }
        try {
          switch (s.provider) {
            case SupportAuthType.EMAIL_PASSWORD:
              this.addContents({ data: "➡ auth.signInWithEmailAndPassword()をawait" });
              await this.fb.auth.signInWithEmailAndPassword(s.email, s.password);
              break;
            case SupportAuthType.GOOGLE_OAUTH:
              break;
            case SupportAuthType.TWITTER_OAUTH:
              break;
            case SupportAuthType.PHONE_AUTH:
            default:
              this.writeLCD(`選べないはずだが...(´_ゝ｀)`, LCDLevel.WARNING);
          }
        } catch (e) {
          switch (e.message) {
            case `The given sign-in provider is disabled for this Firebase project. Enable it in the Firebase ` +
            `console, under the sign-in method tab of the Auth section.`:
              this.writeLCD(`⚠ Firebase Console側で認証方式がOFFになってるっぽいですよ。` +
                `じゃあ何で昇格できんだよ...(´_ゝ｀)`, LCDLevel.WARNING);
              break;
            default:
          }
        } finally {
          x.unsubscribe();
        }
      },
      e => {
        this.writeLCD(`不正アクセスはおやめください...: ${e.message}`, LCDLevel.WARNING);
      }
    );
  }


  /**
   * ================================================================================================================
   * session保持方式の切り替えを行う
   * 
   * 
   * ================================================================================================================
   * @param mode 
   */
  async changePersistentMode(mode: "NONE"|"SESSION"|"LOCAL") {
    this.addContents({ data: "➡ auth.setPersistence(恒久方式変更)をawait" });
    switch (mode) {
      // メモリのみなので、消える
      case "NONE":
        await this.fb.auth.setPersistence(firebase.auth.Auth.Persistence.NONE);
        break;
      // browser/activityをロストしても残る(web: indexedDB)
      case "LOCAL":
        await this.fb.auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        break;
      // WEBのみ, indexedDBに記録. tab/browser変更で消える
      case "SESSION":
      default:
        mode = "SESSION";
        await this.fb.auth.setPersistence(firebase.auth.Auth.Persistence.SESSION);
        break;
    }
    this.ctx.persistentMode = mode;
    this.addContents({ data: `← セッション恒久化方式を ${mode} にしました` });
  }
  

  /**
   * ================================================================================================================
   * お試しログイン開始。
   * 匿名認証を行う。
   * 
   * ================================================================================================================
   * @param mode 
   */
  async tryNow() {
    await this.fb.auth.signInAnonymously();
  }


  /**
   * ================================================================================================================
   * 匿名状態で何らかのデータをFirestoreなどに突っ込む処理。
   * FirestoreにはセキュリティルールとしてID検査や認証状態のガードがあるので、そこがハマるポイントを作る
   * 
   * ================================================================================================================
   * @param mode 
   */
  async bindAnything() {
    alert(`ここがハマる可能性のあるポイントで、${this.ctx.userId} とこのブラウザSession上で` +
    `発生したアクションに紐づく何らかの情報をFirestoreやSessionなどに突っ込んで記録しておき、` + 
    `それをIDとマッチングして、後で本番Userに昇格したときに復元する...みたいなことをやろうとする`);

    try {
      const goships = this.contents.filter(x => x.removed !== "true" && x.data === "🔥 <goship>");
      const d = this.dialog.open(
        GoshipDialog,
        {
          // このComponentのHotコンテキストを別のComponentに参照で渡すと、いろいろ苦労するので注意
          data: <Array<IGoshipSchema>>JSON.parse(
            JSON.stringify(goships.length > 5 ? goships.slice(goships.length-5) : goships))
        });
      const x = d.afterClosed().subscribe(
        async s => {
          // cancel
          if (!s) {
            return;
          }
          try {
            this.addContents({ data: "➡ firestore database の goships コレクションに新規Docを作成中" });
            const db = this.fb.firestore.collection("goships");
            await db.add(<IGoshipSchema>{
              from: this.user.displayName ? this.user.displayName : `匿名`,
              to: "@all",
              ts: this.now(),
              text: `${s}`,
              sns: "twitterリンクとか",
              owner: this.user.uid,
              isAnonymous: this.user.isAnonymous
            });
            this.addContents({ data: "← wrote" });
          } catch (e) {
            this.writeLCD(`ダメな奴です... ${e.message}`, LCDLevel.WARNING);
            console.warn(e);
          } finally {
            x.unsubscribe();
          }
        },
        e => {console.warn(e);}
      );

      // ↓ こういうことしても意味ないことを知らしめるためにやる
      this.user.metadata["hogehoge-data"] = { ts: this.now, date: { "hogeId": this.ctx.userId, "action": "fuga-" } };

    } catch (e) {
      this.writeLCD(`ダメな奴です... ${e.message}`, LCDLevel.WARNING);
      console.warn(e);
    }
  }


  /**
   * ================================================================================================================
   * コンテンツがクリックされたときに噂ならインタラクトするラッパ
   * 
   * 
   * ================================================================================================================
   * @param id 
   */
  onGoshipInteracted(id: number) {
    let t: IUXContent;
    try {
      t = this.contents[id];
      if (t.data === "🔥 <goship>" && (t.owner && t.owner.uid === this.ctx.userId) && t.onClick) {
        t.onClick();
      }
    } catch (e) { console.warn(`Out of index or ....${id}`, e); }
  }
  

  /**
   * ================================================================================================================
   * イベントログにある噂を編集・削除するためのダイアログ制御
   * 
   * 
   * ================================================================================================================
   * @param mode 
   */
  async toggleGoshipEditor(goship: IUXContent) {
    try {
      // owner guard(secondary)
      if (
        !this.ctx.userId ||
        goship.removed === "true" ||
        goship.disalbed || 
        !goship.owner ||
        goship.owner.uid !== this.ctx.userId
      ) {
        console.info(`2ndガード食らってるぞ`, goship);
        return;
      }
      const d = this.dialog.open(GoshipEditorDialog, { data: <IUXContent>JSON.parse(JSON.stringify(goship)) });
      const x = d.afterClosed().subscribe(
        async (s: IUXContent) => {
          // cancel
          if (!s) {
            return;
          }
          try {
            const doc = this.fb.firestore.collection("goships").doc(s.id);
            if (s.removed) {
              this.addContents({ data: "➡ firestore database の goships コレクションを削除中" });
              await doc.delete();
            } else {
              this.addContents({ data: "➡ firestore database の goships コレクションを更新中" });
              await doc.update(<IGoshipSchema>{ text: s.children[2].data });
            }
            this.addContents({ data: "← done" });
          } catch (e) {
            if (e.message = "Missing or insufficient permissions.") {
              this.writeLCD(`Firestoreセキュリティルールにより拒否されました`, LCDLevel.WARNING);
            } else {
              this.writeLCD(`ダメな奴です... ${e.message}`, LCDLevel.ERROR);
            }
            console.warn(e);
          } finally {
            x.unsubscribe();
          }
        },
        e => {console.warn(e);}
      );

    } catch (e) {
      this.writeLCD(`ダメな奴です... ${e.message}`, LCDLevel.WARNING);
      console.warn(e);
    }
  }


  /**
   * ================================================================================================================
   * 本番Userへの昇格処理
   * ================================================================================================================
   * @param mode 
   */
  async registerNow() {
    const ack = confirm("ここでモーダルでメールとかパスとか入力させて...");
    const reg = this.dialog.open(RegisterationDialog, { disableClose: true, hasBackdrop: true, minWidth: "480px" });
    const x = reg.afterClosed().subscribe(
      async s => {
        // canceled
        if (!s || !s.provider) {
          this.writeLCD(`登録・・・してくれない(/_;)`);
        } else {
          switch (s.provider) {
            case SupportAuthType.EMAIL_PASSWORD:
              await this.registerByEmailpass(s.email, s.password);
              break;
            case SupportAuthType.GOOGLE_OAUTH:
              break;
            case SupportAuthType.TWITTER_OAUTH:
              break;
            case SupportAuthType.PHONE_AUTH:
            default:
              this.writeLCD(`選べないはずだが...(´_ゝ｀)`, LCDLevel.WARNING);
          }
          this.ctx.userId = this.user.uid;
          if (s.name) {
            await this.user.updateProfile({ displayName: s.name, photoURL: "" });
            this.ctx.username = this.user.displayName;
          }
        }
        x.unsubscribe();
      },
      e => {
        this.writeLCD(`ほげっ？: ${e.message}`, LCDLevel.WARNING);
      }
    );
  }


  /**
   * ================================================================================================================
   * email/pass方式での昇格
   * ================================================================================================================
   * @param email 
   * @param password 
   */
  protected async registerByEmailpass(email: string, password: string) {
    this.addContents({ data: `➡ 匿名からemail/password方式での昇格を実行`,
      children: [{ data: `(実行前UID: ${this.user.uid})`}] });
    const cred = firebase.auth.EmailAuthProvider.credential(email, password);
    const prevUser = {
      uid: this.user.uid,
      email: this.user.email,
      isAnonymous: this.user.isAnonymous,
      metadata: JSON.stringify(this.user.metadata)
    }
    try {
      const x = await this.user.linkAndRetrieveDataWithCredential(cred);
      this.writeLCD(`おめでとうございます！正式なメンバーとなりました`);

      this.addContents({ data: `← ID  : (${prevUser.uid}) ⇒ (${x.user.uid})` });
      this.addContents({ data: `← Mail: (${prevUser.email}) ⇒ (${x.user.email})` });
      this.addContents({ data: `← Name: (匿名) ⇒ (${x.user.displayName})` });
      this.addContents({ data: `← Anon: (${prevUser.isAnonymous}) ⇒ (${x.user.isAnonymous})` });
      this.addContents({ data: `← Data: ${prevUser.metadata}` });
      // 変わらないんだな、これが 
      this.addContents({ data: `⇒ ${JSON.stringify(x.user.metadata)}` });
    } catch (e) {
      if (e.message === "The email address is already in use by another account.") {
        this.writeLCD(`残念。そのメールアドレス、既に登録されてるみたいよ`, LCDLevel.ERROR);
      }
      this.addContents({ data: `← ✖ イイ感じに失敗するわけだが...` });
    }
  }
}
