
export enum SupportAuthType {
  EMAIL_PASSWORD = "emailpass",
  GOOGLE_OAUTH = "google",
  TWITTER_OAUTH = "twitter",
  PHONE_AUTH = "phone"
}

/** */
export interface IUXContent {
  children?: Array<IUXContent>;
  data: string;
  removed?: string;
  tooltip?: string;
  tooltipPosition?: string;
  customClass?: string;
  onClick?: (ev?: MouseEvent | TouchEvent) => void;
  disalbed?: boolean;
  id?: string;
  owner?: {
    isAnonymous: boolean;  // ★ 要注意設計
    uid: string;
    username?: string;     // ★ 要注意設計
  }
}


/** 噂データモデル: Firestoreのコレクションスキーマ */
export interface IGoshipSchema {
  ts: number;
  text: string;
  from: string;          // ★ 要注意設計
  to: string;            // ★ 要注意設計
  sns: string;
  owner: string;
  isAnonymous: boolean;  // ★ 要注意設計
}
