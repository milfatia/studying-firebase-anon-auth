import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HallComponent } from './index.component';
import { FirebaseModule } from '../firebase/index.module';

import {
  MatButtonModule,
  MatTooltipModule,
  MatDialogModule,
  MatListModule,
  MatFormFieldModule,
  MatOptionModule,
  MatSelectModule,
  MatInputModule
} from "@angular/material";
import { RegisterationDialog } from './registeration-dialog.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { GoshipDialog } from './goship-dialog.component';
import { GoshipEditorDialog } from './goship-editor-dialog.component';

@NgModule({
  declarations: [
    RegisterationDialog,
    GoshipDialog,
    GoshipEditorDialog,
    HallComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule,
    MatTooltipModule,
    MatListModule,
    MatFormFieldModule,
    MatOptionModule,
    MatInputModule,
    MatSelectModule,
    FirebaseModule
  ],
  entryComponents: [
    RegisterationDialog,
    GoshipDialog,
    GoshipEditorDialog
  ],
  providers: [],
  exports: [
    HallComponent
  ]
})
export class HallModule { }
