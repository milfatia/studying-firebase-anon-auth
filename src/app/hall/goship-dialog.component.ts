import { FormControl, Validators } from "@angular/forms";
import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { IUXContent, IGoshipSchema } from "./interfaces";


@Component({
  selector: "goship-dialog",
  template: `
  <h3>そういえばさ……</h3>
  <hr />
  <div class="">
    <div *ngFor="let x of goships">
      <li class="stackable"><ul *ngFor="let y of x.children">{{y.data}}</ul></li>
    </div>
  </div>
  <mat-form-field class="wide-form">
    <input matInput placeholder="(ヒソヒソ…)" [formControl]="fc.chatbox" required> 
    <mat-error *ngIf="fc.chatbox.invalid">{{getErrorMessage()}}</mat-error>
  </mat-form-field>
  <button mat-raised-button color="primary" (click)="sendPost()" [disabled]="!fc.chatbox.valid">噂する</button>
  <button mat-raised-button color="warn" (click)="retireNow()">やっぱやめた</button>
  `,
  styles: [`
    .wide-form {
      min-width: 420px;
    }
  `]
})
export class GoshipDialog {
  /* ui bounds */
  fc = {
    chatbox: new FormControl('', [Validators.required, Validators.maxLength(4096)]),
  };

  /* */
  constructor(
    protected dialog: MatDialogRef<GoshipDialog>,
    @Inject(MAT_DIALOG_DATA) public goships: Array<IUXContent>
  ) {}


  /**
   * ================================================================================================================
   * 何もせずクローズ
   * 
   * 
   * ================================================================================================================
   */
  retireNow() {
    this.dialog.close();
  }


  /**
   * ================================================================================================================
   * 登録内容を返却してクローズ
   * 
   * 
   * ================================================================================================================
   */
  sendPost() {
    this.dialog.close(this.fc.chatbox.value);
  }
  

  /** form validation error resolver */
  getErrorMessage() {
    return this.fc.chatbox.hasError('required') ? 'なんかかけや(´_ゝ｀)' :
      this.fc.chatbox.hasError('maxlength') ? 'なげえよ(;´･ω･)' : '';
  }

}