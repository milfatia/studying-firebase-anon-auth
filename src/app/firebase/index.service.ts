import { Injectable } from "@angular/core";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

import {environment} from "../../environments/environment";

@Injectable()
export class FirebaseService {
  protected readonly app: firebase.app.App;

  constructor() {
    this.app = firebase.initializeApp(environment.firebaseClientCredential);
  }

  get ref() {
    return this.app;
  }

  get auth() {
    return this.app.auth();
  }

  get firestore() {
    const fstore = this.app.firestore();
    fstore.settings({ timestampsInSnapshots: true});
    return fstore;
  }
}